//this is my include file. It has all the function definitions and the struct node

struct node{
   int data;
   struct node *next;
};


void release_node(struct node *node);

struct node *get_node(struct node *node);

struct node *initStack();

struct node *initQueue();

int push(struct node *stack, int number);

void pop(struct node *stack);

int stackEmpty(struct node *stack);

void displayStack(struct node *stack);

int add(struct node *queue, int number);

void remove_q(struct node *queue);

int queueEmpty(struct node *queue);

void displayQueue(struct node *queue);
