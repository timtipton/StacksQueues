//Name: Tim Tipton
//Class: CSE 222
//Date: 2/10/2021
//Assignment: PA3
//Instructor: Nick Macias
//Overview: This program will create and manipulate both a stack and a queue. Things it will be able to do include adding and removing numbers from the queue,
//pushing and popping numbers from the stack, printing both the stack and the queue, and switching between using either the stack or the queue.

#include <stdio.h>
#include <stdlib.h>
#include "codeGod.h"

void main(){
   int stackMode=1;//starts the program in stack mode
   int number;//global variable to keep things consistent
   struct node *stack=initStack();//initializes an empty stack
   struct node *queue=initQueue();//initialzes an empty queue
   while(1){
   char buffer[120];//buffer for fgets
   fgets(buffer, 120, stdin);
   char command;//variable to store the command
   int is_int=sscanf(buffer, "%d", &number);//parses input from fgets, checks if it's an int
   int is_command=sscanf(buffer, "%c", &command);//parses input from fgets, checks to see if it's a char

   if(is_int==1 && stackMode==1){//this is the logic for the basic user interaction with the program. It takes into account what mode you're in and what was pressed
      push(stack, number);
   }
   else if(is_int==1 && stackMode==0){
      add(queue, number);
   }

   else if(is_command==1 && command=='s'){
      displayStack(stack);
      stackMode=1;
   }
   else if(is_command==1 && command=='q'){
      displayQueue(queue);
      stackMode=0;
   }
   else if(is_command==1 && command=='p'){
      int empty_s=stackEmpty(stack);
      int empty_q=queueEmpty(queue);
      if(stackMode==1 && empty_s==1){
         pop(stack);
      }
      else if(stackMode==0 && empty_q==1){
         remove_q(queue);
      }
   }
   else if(is_command==1 && command=='Q'){
      struct node *current_s=stack->next;
      struct node *previous_s=stack;
      struct node *current_q=queue->next;
      struct node *previous_q=queue;

      while(current_s!=NULL){
         free(previous_s);
         previous_s=current_s;
         current_s=current_s->next;
      }
      while(current_q!=NULL){
         free(previous_q);
         previous_q=current_q;
         current_q=current_q->next;
      }
      free(previous_s);
      free(previous_q);
      exit(0);
   }
   else{//prints error statement if the user doesn't input a correct command
      printf("s   prints contents of stack (initiates Stack Mode)\n");
      printf("q   prints contents of queue (initiates Queue Mode)\n");
      printf("#   adds a number to either the stack or queue depending on which mode you're in\n");
      printf("p   pops the top of the stack or removes item from head of the queue\n");
      printf("Q   exits the program\n");
   }}
return;
}  
