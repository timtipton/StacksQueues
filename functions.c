//this is the file with all of my functions for PA3. These functions will be how my main program interacts with the stack and queue.
//#include "codeGod.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
//defines a struct node to be used for stacks and queues
struct node{
   int data;
   struct node *next;
};
//release_node function frees the memory from a deleted node.
//It takes a pointer to that node and has no return value.
void release_node(struct node *node){
   free(node);
}
//get_node function checks the return value of malloc to make sure that there's room for a new node
struct node *get_node(struct node *node){
   struct node *valid=malloc(sizeof(struct node));
   if(valid!=NULL)
      return valid;
   else
      return NULL;
}
//initStack function initializes an empty stack and returns its address
struct node *initStack(){
   struct node *stack;
   stack=malloc(sizeof(struct node));//allocates memory for a struct node and makes stack a pointer to that node
   stack->next=NULL;
   return stack;
}

//initQueue funtion initializes an empty queue and returns its address
struct node *initQueue(){
   struct node *queue;
   queue=malloc(sizeof(struct node));//allocates memory for a struct node and makes queue a pointer to that node
   queue->next=NULL;
   return queue;
}

//push function adds a node to the top of the stack. It returns 1 if successful and 0 if not.
//It takes a pointer to a struct node and an integer as arguments.
int push(struct node *stack, int number){
   struct node *new=get_node(stack);//sets a variable to keep track of the address of the new node
   struct node *current=stack->next;
      if(new!=NULL){//get_node returns NULL if there's no memory, so if there's memory then push will add a number to the top of the stack
         new->data=number;
         new->next=stack->next;//puts the new node on the top of the stack
         stack->next=new;
         return 1;
      }
      else
         return 0;
} 


//pop fuction renmoves a node from the top of the stack.
//It has no return value
//It takes a pointer to the stack
void pop(struct node *stack){
   struct node *current=stack->next;//creates a pointer to the node thet the root is pointing to (NULL for an empty stack)
   stack->next=current->next;
   printf("%d\n", current->data);
   release_node(current);//releases the current node
   return;
}

//my stackEmpty function will return 0 if the stack is empty and 1 if not
int stackEmpty(struct node *stack){
   if(stack->next==NULL)
      return 0;
   else
      return 1;
}
//displayStack funtion will print out the contens of the stack with the top of the stack on the left
//It takes a pointer to the stack and has no return value
void displayStack(struct node *stack){
   struct node *current=stack->next;
   while(current!=NULL){
      printf("%d ", current->data);
      current=current->next;
   }
   printf("\n");
}

//add function will add an integer to the tail of the queue.
//It takes a pointer to the queue and the integer to add.
//It returns 1 if successful and 0 if not.
int add(struct node *queue, int number){      
   struct node *new=get_node(queue);//sets a variable to keep track of the address of the new node
   struct node *current=queue->next;
      if(new!=NULL){//get_node returns NULL if there's no memory, so if there's memory then push will add a number to the top of the stack
         new->data=number;
         new->next=queue->next;//puts the new node on the top of the queue
         queue->next=new;
         return 1;
      }
      else
         return 0;
} 

//remove functon removes a number from the head of the queue
//it takes a pointer to the queue and has no return value
void remove_q(struct node *queue){
   struct node *current=queue->next;  
   struct node *previous=queue;
   while(current->next!=NULL){//traverses the queue until it gets to the head
      previous=current;
      current=current->next;
   }
   previous->next=NULL;
   printf("%d\n", current->data);//prints the data value of the node being removed
   release_node(current);
   return;
}

//queueEmpty function will return a 0 if the queue is empty or a 1 if not
//It takes a pointer to the queue.
int queueEmpty(struct node *queue){
   if(queue->next==NULL)
      return 0;
   else
      return 1;
}

//displayQueue function will print all the data values of the queue.
//It takes a pointer to the queue and has no return value.
void displayQueue(struct node *queue){
   struct node *current=queue->next;
   while(current!=NULL){
      printf("%d ", current->data);
      current=current->next;
   }
   printf("\n");
}






